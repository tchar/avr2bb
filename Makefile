# folder definitions.
# do not define the same folder as BB and BB_SRC
# if you do make clean will delete your source code

# source folder for BeagleBoard code
BB_SRC=src
# executables folder for BeagleBoard
BB=bb
# object folder for BeagleBoard (will be inside BB folder)
BB_OBJ=bb/obj
# source folder for AVR code
AVR_SRC=src
# hex folder for AVR
AVR=avr
# object folder for AVR (will be inside AVR folder)
AVR_OBJ=avr/obj

# make all will compile and link all source code
all: checkfolders bb avr

# make bb will compile and link only BeagleBoard code
bb: bbfolder bbexec

# create BB folder
bbfolder:
	mkdir $(BB)

# link all the objects to executable
bbexec: bbobjfolder bbcode.o sp-proc.o
	gcc -Wall $(BB_OBJ)/bbcode.o $(BB_OBJ)/sp-proc.o -o $(BB)/bbexec

# create BB objects folder
bbobjfolder:
	mkdir $(BB_OBJ)

# create object
bbcode.o:
	gcc -Wall -c $(BB_SRC)/bbcode.c -o $(BB_OBJ)/bbcode.o

# create object
sp-proc.o: 
	gcc -Wall -c $(BB_SRC)/sp-proc.c -o $(BB_OBJ)/sp-proc.o

# make avr will compile and link only AVR code
avr: avrfolder avrexec.hex

# create AVR folder
avrfolder:
	mkdir $(AVR)

# build hex from elf file
avrexec.hex: avrcode.elf
	avr-objcopy -j .text -j .data -O ihex  $(AVR_OBJ)/avrcode.elf $(AVR)/avrexec.hex

# build elf from object files
avrcode.elf: avrobjfolder avrcode.o uart.o
	avr-gcc -mmcu=atmega32 $(AVR_OBJ)/avrcode.o $(AVR_OBJ)/uart.o -o $(AVR_OBJ)/avrcode.elf

# create AVR object folder
avrobjfolder:
	mkdir $(AVR_OBJ)

# create object
avrcode.o:
	avr-gcc -Wall -mmcu=atmega32 -c $(AVR_SRC)/avrcode.c -o $(AVR_OBJ)/avrcode.o

#create object
uart.o:
	avr-gcc -Wall -mmcu=atmega32 -c $(AVR_SRC)/uart.c -o $(AVR_OBJ)/uart.o

clean_bb:checkfoldersbb
	rm -rf $(BB_OBJ)
	rm -rf $(BB)

clean_avr:checkfoldersavr
	rm -rf $(AVR_OBJ)
	rm -rf $(AVR)	

clean:checkfolders
	rm -rf $(BB_OBJ)
	rm -rf $(AVR_OBJ)
	rm -rf $(BB)
	rm -rf $(AVR)

# check folder names
checkfolders: checkfoldersbb checkfoldersavr

# check folder names for BeagleBoard
checkfoldersbb:
	@echo "Checking folders for bb";
ifeq "$(BB_SRC)" "$(BB)"
	@echo "Give different path for BeagleBoard source folder and executables folder";
	exit -1
endif
ifeq "$(BB_SRC)" "$(BB_OBJ)"
	@echo "Give different path for BeagleBoard source folder and objects folder";
	exit -1
endif

checkfoldersavr:
	@echo "Checking folders for avr";
ifeq "$(AVR_SRC)" "$(AVR)"
	@echo "Give different path for AVR source folder and executables folder";
	exit -1
endif
ifeq "$(AVR_SRC)" "$(AVR_OBJ)"
	@echo "Give different path for AVR source folder and objects folder";
	exit -1
endif
