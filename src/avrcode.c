#define F_CPU 16000000UL

#include <avr/io.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include "uart.h"

#define SP_STOP 1
#define SP_START 2
#define IO_DDR DDRA
#define IO_PORT PORTA
#define IO_PIN PINA
#define BT_PRESSED 1
#define BT_UNPRESSED 0

// procedure to return if key 0-7 is pressed or not
// if pressed returns 0, if not returns 1
// works when set triggers to 1
uint8_t isPressed(uint8_t key){
	return (((IO_PIN & (0x01 << key)) >> key) ^ 0x01);
}

// to be run on avr
int main(){
	// Initiate the uart
	uart_init();
	// Set A0-6 as input, A7 as output
	IO_DDR = 0b10000000;
	// Triggers up
	IO_PORT = 0b1111111;
	
	while(1){
		
		// Turn the led on
		IO_PORT |= 0b00000001 << 7;
		// Wait to get signal from BeagleBoard to start sending
		while (uart_getchar() != SP_START);
		
		// Turn the led off
		IO_PORT &= 0b01111111 ; 
			
		while (1){	
			
			// Send something
			uart_sendstring("Hello world ");
			
			// If stop button (pin A1) is pushed send
			// STOP to serial port and break
			if (isPressed(1) == BT_PRESSED){
				uart_sendchar(SP_STOP);
				break;
			} 
		}
	}
}
