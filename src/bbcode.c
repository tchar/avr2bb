#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>
#include "sp-proc.h"

#define MY_BAUDRATE B19200

// definitions for commands
#define SP_STOP 1
#define SP_START 2

// this procedure is being run by child process
// to read data from serial port
void readFromSP(int fd){
    // wait for parent to continue
    raise(SIGSTOP);
    while(1){

        // loop until get q for quit or s to start reading from port
        char *option = (char *) malloc( 20 * sizeof(char));
        while (1){
            printf("Type start <enter> to start reading from port, or quit <enter> to quit: ");
            scanf("%s", option);
            if ( ( strcmp(option,"quit") == 0 ) || ( strcmp(option,"start") == 0 ) ){
                break;
            }
        }

        // exit if option is q
        if ( strcmp( option, "quit" ) == 0 ){
            break;
        }

        // tell AVR to start sending
        char writeSP = SP_START;
        int wordsWrote = write(fd, &writeSP, 1);
        if (wordsWrote < 0){
            perror("Failed to write to serial port");
        }
        else if ( wordsWrote == 0){
            printf("Write returned zero\n");
        }

        // read from port
        int wordsRead;
        char readSP;
        while (1){
            wordsRead = read(fd, &readSP, 1);
            if (wordsRead > 0){
                if (readSP == SP_STOP){
                    printf("\n\n\nStopped from avr\n");
                    break;;
                }
                else
                    printf("%c", readSP);
            }
        }
    }
    exit(0);
}

// main
int main(int argc, char** argv){

    // if wrong arguements
    if (argc != 2){
        printf("Usage: ./bbexec <device_path>\n");
        printf("e.g:./bbexec /dev/ttyUSB0\n");
        printf("If you dont know which is your device type \"dmesg | grep tty\"\n");
        printf("You need to have permissions to open the device.\n");
        printf("To check it type \"ls -l <device-path>\" e.g: \"ls -l /dev/ttyUSB0\"\n");
        exit(-1);
    }

    // create the struct
    struct termios options, oldoptions;

    // set the serial port options
    memset(&options,0,sizeof(options));
    options.c_iflag = 0;
    options.c_oflag = 0;
    // 8 bits, no parity, 1 stop bit
    options.c_cflag = CS8|CREAD|CLOCAL;
    options.c_lflag = 0;

    // read 1 byte
    options.c_cc[VMIN] = 1;
    // wait forever to timeout
    options.c_cc[VTIME] = 0;

    // setting Baud Rate
    cfsetispeed(&options, MY_BAUDRATE);
    cfsetospeed(&options, MY_BAUDRATE);

    // open Serial Port
    int fdSP = openSP(argv[1]);

    // save old options to oldoptions
    getOldAttr(fdSP, &oldoptions);

    // set new options
    setNewAttr(fdSP, &options, &oldoptions);

    // create child to read from port
    int status;
    pid_t pid = fork();
    if (pid < 0){
        perror("Error creating child\n");
        exit(1);
    }
    else if (pid == 0){
        // call procedure to read from serial port
        readFromSP(fdSP);
        exit(0);
    }
    else{
        // print some info
        printf("Created child with pid = %ld to read from Serial Port:%s\n", (long) pid, argv[1]);
        printf("To stop reading from serial port open another terminal ");
        printf("and type sudo kill %ld or press the stop button from avr ok?", (long) pid);
        // wait for enter
        getchar();
        // child continues
        kill(pid, SIGCONT);
        // wait for child to terminate
        wait(&status);
        printf("\nChild terminated with status: %d\n", status);

        // restoring old options
        resetOldAttr(fdSP, &oldoptions);
        // closing SerialPort
        closeSP(fdSP);
        exit(0);
    }
}

