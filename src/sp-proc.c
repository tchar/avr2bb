#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>

// open serial port
int openSP(char * portName){
	int fd= open(portName, O_RDWR | O_NOCTTY | O_NDELAY);
	// on error terminate program
	if (fd == -1){
		perror("Error opening serial port");
		exit(-1);
	}
	return fd;
}

// close serial port
void closeSP(int fd){
	// on error terminate program
	if (close(fd) == -1){
			perror("Error closing port");
			exit(-1);
	}
}

// get the existing attributes from serial port
void getOldAttr(int fd, struct termios* old){
	// on error close the serial port and terminate program
	if (tcgetattr(fd, old) != 0){
		perror("Error getting old options from serial port");
		closeSP(fd);
		exit(-1);
	}
}

// reset the attributes the serial port had before 
// the excecution of the program
void resetOldAttr(int fd, struct termios* old){
	// on error close the serial port and terminate program
	if (tcsetattr(fd, TCSANOW, old) != 0){
		perror("Error restoring old options");
		closeSP(fd);
		exit(-1);
	}
}

// set the new attributes on serial port
void setNewAttr(int fd, struct termios* new, struct termios* old){
	// on error reset the attributes, close the serial port and terminate program
	if (tcsetattr(fd, TCSANOW, new) != 0){
		perror("Error applying settings to serial port");
		resetOldAttr(fd, old);
		closeSP(fd);
		exit(-1);
	}
}
