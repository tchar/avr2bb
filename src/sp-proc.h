// open serial port
int openSP(char* portName);
// close serial port
void closeSP(int fd);
// get existing attributes (old) from serial port
void getOldAttr(int fd, struct termios* old);
// reset attributes to the old ones on serial port
void resetOldAttr(int fd, struct termios* old);
// set new attributes on serial port
void setNewAttr(int fd, struct termios* new, struct termios* old);
