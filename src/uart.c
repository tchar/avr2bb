#include <avr/io.h>

// procedure to initialize UART
void uart_init (void){
    UBRRH = 0;                      
    UBRRL = 51;                           
    UCSRB|= (1<<TXEN)|(1<<RXEN);                
    UCSRC|= (1<<URSEL)|(1<<UCSZ0)|(1<<UCSZ1)|(1<<USBS);   
}

// proccedure to send char (1 byte)
void uart_sendchar (char data){
    while (!( UCSRA & (1<<UDRE)));                
    UDR = data;                                   
}

// procedure to send string
void uart_sendstring(char *s){
	while (*s !=  0){
		uart_sendchar(*s);
		s++;
	}
}

// procedure to receive char (1 byte)
unsigned char uart_getchar (void){
    while(!((UCSRA) & (1<<RXC)));                   
    return UDR;                                   
}

// procedure to receive string
/* To be implemented
 * unsigned char * uart_getstring(void){
 *     char * data;
 *     // do something
 *     return data;
 * }
 */
