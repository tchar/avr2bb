// procedure to initialize UART
void uart_init (void);

// proccedure to send char (1 byte)
void uart_sendchar (char data);

// procedure to send string
void uart_sendstring(char *s);

// procedure to receive char( 1 byte)
unsigned char uart_getchar (void);

// procedure to receive string
/* To be implemented
 * unsigned char * uart_getstring(void);
 */
